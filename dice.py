import random


def usrinteract():
    a = 4
    print('\nAi 4 incercari de a nimeri cifra care va fi afisata pe zar.\n')
    
    while a > 0:
        user = input("Cat crezi ca dai cu zarul? ")
        zar = random.randrange(1, 7)

        try:
            a -= 1
            if int(user) == zar:
                print(zar)
                print("Bravo! ai nimerit!")
                break

            elif zar != int(user) and a > 0:
                print(zar)
                print("Mai incearca.")

            else:
                print(zar)
                print('Imi pare rau, nu ai nimerit.')

        except:
            print('Te rog introdu o valoare numerica cuprinsa intre 1 si 6')


usrinteract()